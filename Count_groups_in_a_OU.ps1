﻿# Define the name of the Organizational Unit (OU) you want to count the groups for
$ouName = "OU=external,OU=user,DC=exak,DC=local"

# Get all the groups within the specified OU and count them
$groupCountInTheOU = (Get-ADGroup -Filter * -SearchBase $ouName -SearchScope OneLevel).count
$groupInTheOU = (Get-ADGroup -Filter * -SearchBase $ouName -SearchScope OneLevel)

$groupCountInTheOUandSubOUs = (Get-ADGroup -Filter * -SearchBase $ouName).count
$groupInTheOUandSubOUs = (Get-ADGroup -Filter * -SearchBase $ouName)

# Output the count of groups found
if ($groupCountInTheOU -ne "0") {
   Write-Host "Number of groups in $ouName is :" $groupInTheOU.Count 
   "and are :" 
   
}
else {
   Write-Host "there is no group in this OU"
}

if ($groupCountInTheOUandSubOUs -ne "0") {
   Write-Host "Number of groups in $ouName and it sub-OUs is :" $groupInTheOUandSubOUs.Count 
   "and are :" 

}
else {
   Write-Host "there is no group in this OU and it sub-OUs"
}
 


#Write-Host "Number of groups in $ouName and it sub OUs are : $groupCountInTheOUandSubOUs"