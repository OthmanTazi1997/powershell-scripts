﻿#Connect-ExchangeOnline -UserPrincipalName admin@contoso.com

$ouName = "OU=user,DC=exak,DC=local"


$emailLegacyDNData = Import-Csv -Path "C:\mailusers_LDN.csv"
#$emailLegacyDNData.count

foreach ($emailLegacyDNRow in $emailLegacyDNData) {

    $emailAddress = $emailLegacyDNRow.EmailAddress
    $legacyDN = $emailLegacyDNRow.LegacyExchangeDN


    $adUser = Get-ADUser -Filter {EmailAddress -eq $emailAddress} -SearchBase $ouName Subtree


    if ($adUser) {
        $proxyAddresses = $adUser.ProxyAddresses
        $proxyAddresses += "x500:$legacyDN"
        Set-ADUser -Identity $adUser -Replace @{ProxyAddresses=$proxyAddresses}
    }
}

