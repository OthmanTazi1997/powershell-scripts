﻿function Get-OUAndSubOUinfo{

    param(
        [string[]]$OUDN, $type)

#=========================================================================================================    

    $subOU = (Get-ADOrganizationalUnit -Filter * -SearchBase "$OUDN" -SearchScope Subtree)

#=========================================================================================================

    if($type -eq "group"){

        foreach ($INOU in $subOU){
            $GRPs = (Get-ADGroup -Filter * -SearchBase "$INOU" -SearchScope OneLevel).name
            #$GRPs
            #$GRPs.count
            #$INOU.name
            [pscustomobject]@{
                OU = $INOU.name
                nbr_of_Groups = $GRPs.count
                Groups = $GRPs}             
        }
    }


#=========================================================================================================

    if($type -eq "user"){
        
        foreach ($INOU in $subOU){
            $Users = (Get-ADUser -Filter * -SearchBase "$INOU" -SearchScope OneLevel).name
            #$Users
            #$Users.count
            #$INOU.name
            [pscustomobject]@{
                OU = $INOU.name
                nbr_of_Users = $Users.count
                Users = $Users}             
        }
    }

#=========================================================================================================

    if($type -eq "all"){
        
        foreach ($INOU in $subOU){

            $Users = (Get-ADUser -Filter * -SearchBase "$INOU" -SearchScope OneLevel).name
            $GRPs = (Get-ADGroup -Filter * -SearchBase "$INOU" -SearchScope OneLevel).name
            
            [pscustomobject]@{
                OU = $INOU.name
                nbr_of_Users = $Users.count
                Users = $Users
                nbr_of_Groups = $GRPs.count
                Groups = $GRPs}             
        }
    }


}