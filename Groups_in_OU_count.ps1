﻿function Get-OUAndSubOUGroupCount{
    param(
        [string[]]$OUDN
    )

    #$groupCountInTheOU = (Get-ADGroup -Filter * -SearchBase $OUDN -SearchScope OneLevel).count
    $groupInTheOU = (Get-ADGroup -Filter * -SearchBase "$OUDN" -SearchScope OneLevel)

    #$groupCountInTheOUandSubOUs = (Get-ADGroup -Filter * -SearchBase $OUDN).count
    $groupInTheOUandSubOUs = (Get-ADGroup -Filter * -SearchBase "$OUDN")



    if ($groupInTheOU.count -ne "0") {
        Write-Host "Number of groups in $OUDN is :" $groupInTheOU.count 
        "and are :" 
        $groupInTheOU    
    }
    else {
        Write-Host "there is no group in this OU $groupInTheOU"
    }




    if ($groupInTheOUandSubOUs.count -ne "0") {
        Write-Host "Number of groups in $OUDN is :" $groupInTheOUandSubOUs.count 
        "and are :" 
        $groupInTheOUandSubOUs    
    }
    else {
        Write-Host "there is no group in this OU and it sub OUs"
    }

}