﻿#connect to exchange online
#Get-Mailbox -Identity  "inter.user1" | Select LegacyExchangeDN

$ouName = "OU=user,DC=exak,DC=local"

$mailboxes = Get-MailUser -ResultSize Unlimited | Where-Object {$_.EmailAddresses -like "*@exocloud.uk" -and $_.objectClass -eq "user"}
#$mailboxes.count

foreach ($mailbox in $mailboxes) {

    $emailAddress = $mailbox.PrimarySmtpAddress.ToString()
    #$emailAddress

    $adUser = Get-ADUser -Filter {EmailAddress -eq $emailAddress} -SearchBase $ouName
    #$adUser.name
 
    if ($adUser) {
        #$proxyAddresses = $adUser.ProxyAddresses
        #$proxyAddresses += "x500:$($mailbox.LegacyExchangeDN)"

        $UproxyAddresses = "x500:$($mailbox.LegacyExchangeDN)"
        Set-ADUser -Identity $adUser -Add @{ProxyAddresses=$UproxyAddresses}
    }

    }
    
