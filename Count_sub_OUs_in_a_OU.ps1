﻿# Define the name of the Organizational Unit (OU) you want to count the sub-OUs for
$ouName = "OU=user,DC=exak,DC=local"

# Get all the sub-OUs within the specified OU and count them
$ouCount = (Get-ADOrganizationalUnit -Filter * -SearchBase $ouName -SearchScope Subtree)

# Output the count of sub-OUs found

$ouCount.name