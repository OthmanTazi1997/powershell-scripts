﻿Import-Module ActiveDirectory

$ouName = "OU=user,DC=exak,DC=local"

$users = Get-ADUser -Filter * -Properties DisplayName, UserPrincipalName -SearchBase $ouName -SearchScope Subtree


foreach ($user in $users) {
    $newUPN = $user.DisplayName.Replace(" ", ".") + "@" + (Get-ADDomain).DNSRoot
    Set-ADUser $user -UserPrincipalName $newUPN
    Write-Host "Updated UPN for user $($user.DisplayName) to $newUPN"
}