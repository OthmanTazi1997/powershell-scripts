﻿Import-Module ActiveDirectory

$ouName = "OU=tete,OU=intern,OU=user,DC=exak,DC=local"

$users = Get-ADUser -Filter {mail -eq "local.user1@exocloud.uk"} -Properties mail -SearchBase $ouName -SearchScope Subtree


foreach ($user in $users) {

    
    $email = $user.mail

    
    $username = $email.Split("@")[0]

    $username
    
    $newupn = $username + "@" + $email.Split("@")[1]

    
    Set-ADUser -Identity $user -UserPrincipalName $newupn
}


Write-Output "UPN prefixes updated for all users."